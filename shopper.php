<!DOCTYPE html>
<html lang="en" class="has-background-primary">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>ASS2</title>
	<link rel="stylesheet" 
    href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.0/css/bulma.min.css">
    <link rel="stylesheet" href="css/bulma.css" type="text/css"/>
	<script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
	<!-- Style calls to be used throughout -->
	<style>
		.center {
			display: flex;
			justify-content: center;
			align-items: center;
		}

		.emoticon { 
			white-space: pre; 
		}
	</style>
</head>
<body>
	<?php 
		// DB connect and disconnect calls
		include("dbconnect.php"); 
		include("dbdisconnect.php"); 	
	?> 



<!-- NAv bar section including mobile and desktop options -->
	<nav class="navbar" role="navigation" aria-label="main navigation">
		<div class="navbar-brand">
		    <a class="navbar-item" href="https://bulma.io">
		    	<img  width="50" height="60">
		    </a>
		    <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="mainNavbar">
		      <span aria-hidden="true"></span>
		      <span aria-hidden="true"></span>
		      <span aria-hidden="true"></span>
		    </a>
		</div>

	    <div id="mainNavbar" class="navbar-menu">
	    	<!-- RBAC to show/hide navbar items based on user security  -->
			<div class="navbar-start">
	      		<a class="navbar-item" href="index.php">Current Items</a>
		        <a class="navbar-item" href="addNew.php">Add Item</a>
			  	<a class="navbar-item" href="categories.php">Categories </a>
			  	<a class="navbar-item" href="addCategory.php">Add Categories</a>
			  	<a class="navbar-item" href="shopper.php">Shopper </a>
			  	<a class="navbar-item" href="product.php">Product </a>
		    </div>

		    <div class="navbar-end">
		      <div class="navbar-item">
		        <div class="buttons">
		          <a class="button is-light">Log out</a>
		        </div>
		      </div>
		    </div>
		</div>
	</nav>
<!-- End Navbar -->



	<section class="section">
		<div class="container">
			<h1 class="title is-1 has-text-white">Catalogue</h1>
			<main>
				<?php 
				    //select items from product
					$sql ="SELECT * FROM product";
					$result = $conn->query($sql);
					if ($result->num_rows > 0) {
						while($row = $result->fetch_assoc()) {
				?>
				<div class="card-content has-background-white">
					<div class="columns">
						<!-- product image using url pulled from DB -->
						<div class="column is-one-fifth">
							<div class="control">
								<img src="<?php echo $row["prod_img_url"] ?>" width="100%">
							</div>
						</div>
						<!--Display selected items product info -->
						<div class="column">
							<h3 class="catalogue-item-title title is-3"><?php echo $row["prod_name"]?> ($<?php echo$row["prod_price"]?>)</h3><div class="control">
									<span class="has-text"><strong>Description: </strong><?php echo $row["prod_desc"]?></span>
								</div>
								<br>
								<div class="column is-one-fifth field is-grouped">
									<div class="control"><input type="number" class="catalogue-item-qty input" value="1" min="1"> </div>
									<input type="submit" value="Add to Cart" class="button is-primary is">
								</div>	
							<!-- line to divide products	 -->
							<hr>
						</div>
							<br>
					</div>
				</div>
				<?php
					}
					} else{
			   		 	echo "0 results";
						}
					?>
				

				
			</main>
		</div>		
	</section>







<script type="text/javascript">
// Drop down menu from burger
	(function() {
		var burger = document.querySelector('.burger');
		var nav = document.querySelector('#'+burger.dataset.target);
		burger.addEventListener('click',function(){
			burger.classList.toggle('is-active');
			nav.classList.toggle('is-active');
		});
	})();
</script>

</body>


</html>