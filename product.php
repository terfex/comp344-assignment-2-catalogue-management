<!DOCTYPE html>
<html lang="en" class="has-background-primary">
<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>ASS2</title>
		<link rel="stylesheet" 
        href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.0/css/bulma.min.css">
        <link rel="stylesheet" href="css/bulma.css" type="text/css"/>
        <link rel="stylesheet" href="css/rating.css" type="text/css"/>
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/jquery-ui.js"></script>
        <script src="js/final.js"></script>
		<script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
        <!-- <link rel="stylesheet" href="css/debug.css"> -->
	<style>
		.center {
			display: flex;
			justify-content: center;
			align-items: center;
		}

		.emoticon { 
			white-space: pre; 
		}
	</style>
</head>
<body>




<!-- NAv bar section including mobile and desktop options -->
	<nav class="navbar" role="navigation" aria-label="main navigation">
		<div class="navbar-brand">
		    <a class="navbar-item" href="https://bulma.io">
		    	<!-- Spot to put company logo if needed -->
		    	<img  width="50" height="60">
		    </a>
		    <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="mainNavbar">
		      <span aria-hidden="true"></span>
		      <span aria-hidden="true"></span>
		      <span aria-hidden="true"></span>
		    </a>
		</div>

	    <div id="mainNavbar" class="navbar-menu">
			<div class="navbar-start">
	      		<a class="navbar-item" href="index.php">Home</a>
		        <a class="navbar-item" href="addNew.php">Add New</a>
		      	<a class="navbar-item" href="discounts.php">Discounts</a>
			  	<a class="navbar-item" href="categories.php">Categories </a>
			  	<a class="navbar-item" href="shopper.php">Shopper </a>
		    </div>

		    <div class="navbar-end">
		      <div class="navbar-item">
		        <div class="buttons">
		          <a class="button is-light">Log out</a>
		        </div>
		      </div>
		    </div>
		</div>
	</nav>
<!-- End Navbar -->



	<section class="section">
		<div class="container">
			<h1 class="title is-1 has-text-white">Catalogue</h1>
			<main>
				<div class="hero-body">
                    <div class="container has-text-centered">
                        <div class="columns is-vcentered">
                            <div class="column is-5">
                                <figure class="image is-4by3">
                                    <img src="https://picsum.photos/800/600/?random" alt="Description">
                                </figure>
                            </div>
                            <div class="column is-6 is-offset-1">
                                <h1 class="title is-2">
                                    Superhero Scaffolding
                                </h1>
                                <h2 class="subtitle is-4">
                                    Let this cover page describe a product or service.
                                </h2>
                                <br>
                                <p class="has-text-centered">
                                    <a class="button is-medium is-info is-outlined">
                        Learn more
                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Put your product ID here, i.e. $_GET['product_id']-->
                <?php
                if (!isset($_GET['product'])){
                    $product_id = "";
                }
                else{
                    $product_id = $_GET['product'];
                }
                ?>
                <input type='hidden' id='product_id' value='<?php echo $product_id?>'/>
                <!--User ID automatically generated based on current session-->
                <input type='hidden' id='user_id'/>
                <div id='recommendationsContainer'>
                </div>
                <div class="hero-foot">
                    <div class="container">
                        <div class="tabs is-centered">
                            <ul>
                                <li><a>And this is the bottom</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
				
			</main>
		</div>		
	</section>







<script type="text/javascript">
// Drop down menu from burger
	(function() {
		var burger = document.querySelector('.burger');
		var nav = document.querySelector('#'+burger.dataset.target);
		burger.addEventListener('click',function(){
			burger.classList.toggle('is-active');
			nav.classList.toggle('is-active');
		});
	})();
</script>

</body>


</html>