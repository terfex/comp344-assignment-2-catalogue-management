var errorLoadingReviews = false;
var errorSendingReview = false;
var star_ratingFlag = false;
var getRecco = false;
var userLoggedIn = false;
var user_id_flag = false;
var formSubmitted = false;

var userReview = "";
var reviewLimit = 0;
var reviewMax = 3;
var reviewCurrent = 0;
var currentReviews;
var recoMax = 3;
var recoCurrent = 0;
var userReviewSent = "";
var user_id = "";
var star_rating = 3;
var currentProduct = 0;
var current_review = 0;
var reviewGetLength = 0;
var reviewNum = 0;

//This small function checks to make sure that the page has been loaded, once the page is complete it calls init which initialises everything
//The if statement is to ensure that this is only called once per page load to save on redundant calls
//Gets the CSRF (secruity) and starts the second script (star-rating). It also gets the reviews for system. It is in a promise state
$(document).ready(function () {
    //Flag to ensure that the script isnt loaded over and over again
    if (star_ratingFlag == false) {
        startStarRating();
        setTimeout(function () {
            insertHTML();
        }, 500);
        setTimeout(function(){
            //load csrf into the page
            getCSRF();
        }, 1000);

    }
    var user_id = getUserID();
    function getUserID(){
        var user_id = $.post("https://platypus.science.mq.edu.au/~44636911/comp344/live/index.php?mode=user&action=get");
        user_id.then(function (data) {
            setTimeout(function(){
                $("#user_id").val(JSON.parse(data));
                user_id = JSON.parse(data);
            }, 500)
        });
        return user_id;
    }
    //Return the amount of reviews
    function getReviewNum(){
        var review_num = $.post("https://platypus.science.mq.edu.au/~44636911/comp344/live/index.php?mode=reviews&action=count", {product_id: $("#product_id").val()});
        review_num.then(function (data) {
            setTimeout(function(){
                reviewNum = JSON.parse(data);
                getReviews(reviewMax);
            }, 500)
        });
    }
    //Load user id for transactions by using current session
    if (user_id_flag == false) {
        user_id = $.post("https://platypus.science.mq.edu.au/~44636911/comp344/live/index.php?mode=user&action=get");
        user_id.then(function (data) {
            setTimeout(function(){
                $("#user_id").val(JSON.parse(data));
                //Get the user's review (if they have posted one!)
                getReview();
                //Get appropriate star ratings for the user
                loadStars();
                //Get all recommendations for the user
                getRecommendations();
                //Get all reviews
                getReviewNum();
            }, 500);
        });
        user_id_flag = true;
    }

    //catches the person submitting the form and ensures that the javascript has time to parse the information
    setTimeout(function(){
        var get_user_review = "";
        //Check if user has posted review, if so; hide the box.
        $('#storeReview').click(function () {
            if (formSubmitted == false) {
                checkBeforeSend();
            }
        });
    }, 2000);

});

function getReview(){
    //Retrieve the user's review (if it exists)
    $.ajax({
        type: 'POST',
        url: 'https://platypus.science.mq.edu.au/~44636911/comp344/live/index.php?mode=review&action=get',
        data: {product_id: $("#product_id").val(), user_id: $("#user_id").val()},
        dataType: 'json',
        async: true,
        success: function (data) {
            get_user_review = data;
            if (get_user_review.review != []){
                $("#user_review_tab").hide();
            }
            return get_user_review;
        }
    });
}
function toggleLoader(show){
    if (!show){
        $("#loading-overlay").fadeOut();
    }
    else{
        $("#loading-overlay").fadeIn();
    }
}
function moreReviews() {
    //reviewMax += 3;
    toggleLoader(true);
    getReviews(reviewCurrent);
}

//inserts the bulk of the HTML into the main page to ensure that reviews and recommendations is a contained unit
function insertHTML() {
    $('head').prepend("<meta name='csrf-token' content=''><link href='https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' rel='stylesheet'>" +
        "<link rel='stylesheet' type='text/css' href='css/rating.css'>");
    $('#recommendationsContainer').load('/rating-template.html');
}

//begins the second script, star-rating, once the page has been loaded
function startStarRating() {
    // $('#starRating').attr({type: 'text/javascript', src: 'assets/js/Star-Rating.js'});

    star_ratingFlag = true;
}

//Gets the CSRF string and transfers it to the HTML file
function getCSRF() {
    var token = $.post("https://platypus.science.mq.edu.au/~44636911/comp344/live/index.php?mode=csrf&action=generate");
    token.then(function (data) {
        $("[name='csrf-token']").attr("content", JSON.parse(data));
    });
}

//Gets the recomendations from the server. Depending on whether the user is logged in or not depends on what is send
function getRecommendations() {
    $.ajax({
        type: 'POST',
        url: 'https://platypus.science.mq.edu.au/~44636911/comp344/live/index.php?mode=recommendations&action=get',
        data: {item_id: $("#product_id").val(), quantity: recoMax, user_id: ""},
        // data: {item_id: $("#product_id").val(), quantity: recoMax, user_id: $("#user_id").val()},
        dataType: 'json',
        success: function (data) {
            printRecommendations(data);
            getRecco = true;
        }
    });


}
//Puts the recommendations into their place
function printRecommendations(recoGet) {
    if (recoGet.length != 0) {
        $.each(recoGet, function (index, element) {
            $("#product-recommendations").append("" +
                "<ul class='recoBody'>" +
                "<li class='recoContent' id='recommendationID_0'>" +
                " <!-- need to get reviews through ID-->" +
                "<div class='recoContent-1'>" +
                "<img src='https://platypus.science.mq.edu.au/~43290477/ASS2/images/" + element.prod_img_url + "' class='recommendationImg'/>" +
                "<p class='recommendationName'>" + element.prod_name + "</p>" +
                "<br>" +
                "<p class='recommendationDescription'>" + element.prod_desc.replace(/^(.{100}).+/, "$1&hellip;") + "</p>" +
                "<a href='#' class='recommendationBtn'>View Product</a>" +
                "</div>" +
                "</li>" +
                "</ul>").fadeIn();
        });
    }
    else{
        //Hide recommendations
        $("#recommendations").hide();
        $("#recommendations-title").hide();
    }

}


//gets all of the reviews. Starting with the first 10. Will return less if less are availiable.
function getReviews(x) {
    try {
        $.ajax({
            type: 'POST',
            url: 'https://platypus.science.mq.edu.au/~44636911/comp344/live/index.php?mode=reviews&action=get',
            data: {min: reviewCurrent, max: reviewMax, product_id: $("#product_id").val()},
            dataType: 'json',
            success: function (data) {
                var reviewGet = data;
                //gives time for the spots to be loaded before it is populated
                setTimeout(function () {
                    pasteReviews(reviewGet);
                }, 500);

            }
        });

    } catch (e) {
        errorLoadingReviews = true;
        console.log("ERROR WITH GETTING THE REVIEW! " + e);
    }

}
//Puts the reviews into their specific places.
//ReviewGet is the JSON list of the reviews
//Count is the amount of on screen review slots
function pasteReviews(reviewGet) {
    var reviews = reviewGet.review;
    reviewGetLength = reviewGet.review.length;
    reviewCurrent += 3;
    //Check if there are anymore reviews
    if (reviewCurrent >= parseInt(reviewNum)) {
        $('#more').hide();
    }
    var new_review = "";
    try {
        new_review = "";
        for (var i = 0; i < reviewGetLength; i++) {
            current_review++;
            new_review += " <li class='reviewContent' id='reviewID_" + current_review + "'>" +
                <!-- need to get reviews through ID-->
                "<div class='reviewContent-1'>" +
                "<star-rating value='" + reviewGet.review[i].Rating_Value + "' id='rating'></star-rating><div class='reviewStar'></div>" +
                "<p>by " +
                "<a class='author' href='#'>" + reviewGet.review[i].sh_username + "</a>" +
                "<span class='reviewdate'> " + reviewGet.review[i].Rating_Date +"</span></p>" +
                "</div>" +
                "<div class='reviewContent-2'>" +
                "<p class='reviewTitle'><strong>" + reviewGet.review[i].prod_name + "</strong></p>" +
                "<p class='reviewComment'>" + reviewGet.review[i].Rating_Review + "</p>" +
                "</div>" +
                "</li>";
        }
        setTimeout(function(){
            $("#reviewBody").append(new_review);
            $("#review_count").text(current_review);
            $("#review").fadeIn();
            toggleLoader(false);
        }, 500);


    } catch (e) {
        console.log("An error occurred in final.js around line 249. No more Reviews ");
        console.log(e);
    }
}


//sends the review to the server. There is a slight delay with submitting and sending as the user has to aquire a CSRF token before it is allowed.
function sendReview() {
    if (formSubmitted == false) {
        setTimeout(function () {
            try {
                userReviewSent = $.post("https://platypus.science.mq.edu.au/~44636911/comp344/live/index.php?mode=review&action=add", {
                    user_id: $("#user_id").val(),
                    star_rating: star_rating,
                    product_id: $("#product_id").val(),
                    review: $("#review_data").val()
                }).done(function(msg) {
                    if (msg.error != ""){
                        //Uhoh ! Lets catch this error and display it to the user
                        printFeedback(msg.error, false);
                    }
                    else{
                        formSubmitted = true;
                        $('#submit-review').hide();
                        $("#user_review_tab").hide();
                        openTab(event, 'review');
                        printFeedback("Successfully added review!", true);
                    }
                });
            } catch (e) {
                console.log(e);
                Update();
            }

        }, 200);

    }

}

function printFeedback(msg, success){
    $("#response p").text("");
    $("#response p").text(msg);
    $("#response").removeClass();
    if (success) {
        $("#response").addClass("success");
    }
    else{
        $("#response").addClass("failure");
    }
    $("#response").fadeIn(500);
    setTimeout(function(){
        $("#response").fadeOut(500);
    }, 2000)
}

function checkBeforeSend(){
    //checks if empty and checks if a star has been selected
    if(stringTextArea(document.getElementById('review_data').value) == '' || document.getElementById('ratingSubmit').getAttribute('value') == 'null'){
        alert('Please fill in a review or select a star.');

    }else{
        sendReview(); //change to submit function
    }
}

function stringTextArea(splitString){
    //splits string to check if empty
    if(splitString.substring(0,1) == ' '){
        splitString = splitString.substring(1, splitString.length);
    }
    if(splitString.substring(splitString.length-1, splitString.length) == ' '){
        splitString = splitString.substring(0, splitString.length-1);
    }
    return splitString;
}

function loadStars(){
        try {
            $.ajax({
                type: 'POST',
                url: 'https://platypus.science.mq.edu.au/~44636911/comp344/live/index.php?mode=review&action=stars',
                data: {product_id: $("#product_id").val()},
                dataType: 'json',
                success: function (data) {
                    var one_star = data[0].one_star;
                    var two_star = data[0].two_star;
                    var three_star = data[0].three_star;
                    var four_star = data[0].four_star;
                    var five_star = data[0].five_star;
                    var total_stars = data[0].total_stars;

                    $("#p1").css('width', (one_star / total_stars) * 100 + "%");
                    $("#p2").css('width', (two_star / total_stars) * 100 + "%");
                    $("#p3").css('width', (three_star / total_stars) * 100+ "%");
                    $("#p4").css('width', (four_star / total_stars) * 100+ "%");
                    $("#p5").css('width', (five_star / total_stars) * 100 + "%");
                    //Update all loading bars and prepare average for ratings
                    var average_stars = ((one_star * 1) + (two_star * 2) + (three_star * 3) + (four_star * 4) + (five_star * 5)) / total_stars;
                    $("#average-rating").text(Math.round(average_stars * 100) / 100);
                    $("#review-count").text(total_stars);

                }
            });

        } catch (e) {
            errorLoadingReviews = true;
            console.log("ERROR WITH GETTING THE STARS! " + e);
        }
}


class StarRatingSubmit extends HTMLElement {
    //Only works on latest versions of Mozilla Firefox and Google Chrome as it uses Custom Elements
    get value() {
        return this.getAttribute('value');
    }

    set value(val) {
        this.setAttribute('value', val);
        this.highlight(this.value - 1);
    }

    //sets colour of stars
    highlight(index) {
        this.stars.forEach((star, i) => {
            star.classList.toggle('full', i <= index);
        });
    }

    constructor() {
        super();

        this.stars = [];
        //create # amount of stars
        for (let i = 0; i < 5; i++) {
            let s = document.createElement('div');
            s.className = 'star';
            this.appendChild(s);
            this.stars.push(s);
        }

        this.value = this.value

        //when mouse hovers over star, star changes colour
        this.addEventListener('mousemove', e => {
            let box = this.getBoundingClientRect(),
                starIndex = Math.floor((e.pageX - box.left) / box.width * this.stars.length);

            this.highlight(starIndex);
        });


        this.addEventListener('mouseout', () => {
            this.value = this.value
        });

        this.addEventListener('click', e => {
            let box = this.getBoundingClientRect(),
                starIndex = Math.floor((e.pageX - box.left) / box.width * this.stars.length);
            star_rating = $("#ratingSubmit")[0].value;
            this.value = starIndex + 1;
            star_rating = parseInt(this.value);
            let rateEvent = new Event('rate');
            this.dispatchEvent(rateEvent);
        })
    }
}

customElements.define('star-rating-submit', StarRatingSubmit);


class StarRating extends HTMLElement {

    get value() {
        return this.getAttribute('value');
    }

    set value(val) {
        this.setAttribute('value', val);
        this.highlight(this.value - 1);
    }

    //sets colour of stars
    highlight(index) {
        this.stars.forEach((star, i) => {
            star.classList.toggle('full', i <= index);
        });
    }

    constructor() {
        super();

        this.stars = [];
        //create # amount of stars
        for (let i = 0; i < 5; i++) {
            let s = document.createElement('div');
            s.className = 'star';
            this.appendChild(s);
            this.stars.push(s);
        }

        this.value = this.value
    }
}
customElements.define('star-rating', StarRating);

function openTab(evt, tabName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tab-links");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " active";
}

