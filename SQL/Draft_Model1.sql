-- MySQL Script generated by MySQL Workbench
-- Sun Sep 23 22:57:52 2018
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `mydb` ;

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`Product`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Product` ;

CREATE TABLE IF NOT EXISTS `mydb`.`Product` (
  `prod_id` INT NOT NULL,
  `prod_name` VARCHAR(40) NOT NULL,
  `prod_desc` VARCHAR(128) NULL DEFAULT NULL,
  `prod_img_url` VARCHAR(128) NULL DEFAULT NULL,
  `prod_long_desc` VARCHAR(256) NULL DEFAULT NULL,
  `prod_sku` CHAR(16) NULL DEFAULT NULL,
  `prod_disp_cmd` VARCHAR(128) NULL DEFAULT NULL,
  PRIMARY KEY (`prod_id`),
  INDEX `prod_name` (`prod_name` ASC));


-- -----------------------------------------------------
-- Table `mydb`.`Attribute`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Attribute` ;

CREATE TABLE IF NOT EXISTS `mydb`.`Attribute` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  `Product_prod_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `fk_Attribute_Product1_idx` (`Product_prod_id` ASC),
  CONSTRAINT `fk_product_id`
    FOREIGN KEY (`Product_prod_id`)
    REFERENCES `mydb`.`Product` (`prod_id`));


-- -----------------------------------------------------
-- Table `mydb`.`AttributeValue`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`AttributeValue` ;

CREATE TABLE IF NOT EXISTS `mydb`.`AttributeValue` (
  `AttrVal_id` INT NOT NULL,
  `AttrVal_Value` VARCHAR(45) NOT NULL,
  `AttrVal_Attr_id` INT NOT NULL,
  `AttrVal_Prod_id` INT NOT NULL,
  PRIMARY KEY (`AttrVal_id`),
  INDEX `fk_AV_Attribute_idx` (`AttrVal_Attr_id` ASC),
  INDEX `fk_Prod_id_idx` (`AttrVal_Prod_id` ASC),
  CONSTRAINT `fk_Attr_Id`
    FOREIGN KEY (`AttrVal_Attr_id`)
    REFERENCES `mydb`.`Attribute` (`id`),
  CONSTRAINT `fk_Prod_id`
    FOREIGN KEY (`AttrVal_Prod_id`)
    REFERENCES `mydb`.`Product` (`prod_id`));


-- -----------------------------------------------------
-- Table `mydb`.`category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`category` ;

CREATE TABLE IF NOT EXISTS `mydb`.`category` (
  `cat_id` INT NOT NULL,
  `cat_name` VARCHAR(40) NOT NULL,
  `cat_desc` VARCHAR(128) NULL DEFAULT NULL,
  `cat_img_url` VARCHAR(128) NULL DEFAULT NULL,
  `CAT_DISP_CMD` VARCHAR(128) NULL DEFAULT NULL,
  PRIMARY KEY (`cat_id`),
  INDEX `cat_name` (`cat_name` ASC));


-- -----------------------------------------------------
-- Table `mydb`.`CgPrRel`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`CgPrRel` ;

CREATE TABLE IF NOT EXISTS `mydb`.`CgPrRel` (
  `Id` INT NOT NULL,
  `CgPr_cat_id` INT NOT NULL,
  `CgPr_prod_id` INT NOT NULL,
  PRIMARY KEY (`Id`),
  INDEX `pk_parent_cat_idx` (`CgPr_cat_id` ASC),
  INDEX `pk_child_prod_idx` (`CgPr_prod_id` ASC),
  CONSTRAINT `pk_parent_cat`
    FOREIGN KEY (`CgPr_cat_id`)
    REFERENCES `mydb`.`category` (`cat_id`),
  CONSTRAINT `pk_child_prod`
    FOREIGN KEY (`CgPr_prod_id`)
    REFERENCES `mydb`.`Product` (`prod_id`));


-- -----------------------------------------------------
-- Table `mydb`.`CgryRel`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`CgryRel` ;

CREATE TABLE IF NOT EXISTS `mydb`.`CgryRel` (
  `Id` INT NOT NULL,
  `cgryrel_id_parent` INT NOT NULL,
  `cgryrel_id_child` INT NOT NULL,
  `cgryrel_sequence` INT NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `pk_cc_parent_cat_idx` (`cgryrel_id_parent` ASC),
  INDEX `pk_cc_child_cat_idx` (`cgryrel_id_child` ASC),
  CONSTRAINT `fk_parent_cat`
    FOREIGN KEY (`cgryrel_id_parent`)
    REFERENCES `mydb`.`category` (`cat_id`),
  CONSTRAINT `fk_child_cat`
    FOREIGN KEY (`cgryrel_id_child`)
    REFERENCES `mydb`.`category` (`cat_id`));


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
