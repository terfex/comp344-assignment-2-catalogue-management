<!DOCTYPE html>
<html lang="en" class="has-background-primary">
<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>ASS2</title>
		<link rel="stylesheet" 
        href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.0/css/bulma.min.css">
        <link rel="stylesheet" href="css/bulma.css" type="text/css"/>
		<script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
        <!-- <link rel="stylesheet" href="css/debug.css"> -->
	<style>
	/*styling calls to be used throughout*/
		.center {
			display: flex;
			justify-content: center;
			align-items: center;
		}

		.emoticon { 
			white-space: pre; 
		}
	</style>
</head>

<body>
	<nav class="navbar" role="navigation" aria-label="main navigation">
		<div class="navbar-brand">
		    <a class="navbar-item" href="https://bulma.io">
		    	<!-- Spot to put company logo if needed -->
		    	<img  width="50" height="60">
		    </a>
		    <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="mainNavbar">
		      <span aria-hidden="true"></span>
		      <span aria-hidden="true"></span>
		      <span aria-hidden="true"></span>
		    </a>
		</div>

	    <div id="mainNavbar" class="navbar-menu">
			<!-- RBAC to show/hide navbar items based on user security  -->
			<div class="navbar-start">
	      	<a class="navbar-item" href="index.php">Current Items</a>
		        <a class="navbar-item" href="addNew.php">Add Item</a>
			  	<a class="navbar-item" href="categories.php">Categories </a>
			  	<a class="navbar-item" href="addCategory.php">Add Categories</a>
			  	<a class="navbar-item" href="shopper.php">Shopper </a>
			  	<a class="navbar-item" href="product.php">Product </a>
		    </div>

		    <div class="navbar-end">
		      <div class="navbar-item">
		        <div class="buttons">
		          <a class="button is-light">Log out</a>
		        </div>
		      </div>
		    </div>
		</div>
	</nav>

	<section class="section">
		<div class="container">
			<h1 class="title is-1 has-text-white"> Current Items</h1>
			<main>
					<?php 
						//db connect and disconnect calls
						include("dbconnect.php"); 
						include("dbdisconnect.php");
						
						//select items from product
						$sql ="SELECT * FROM product";
						$result = $conn->query($sql);
						if ($result->num_rows > 0) {
		    				while($row = $result->fetch_assoc()) {
		    						?>
					
					<!-- styling -->
					<div class="card-content has-background-white">
						<div class="columns">

							<div class="column is-one-fifth">
								<div class="control">
									<!-- Display image using img url stored in DB -->
									<img src="<?php echo $row["prod_img_url"] ?>" width="100%">
								</div>
							</div>
								<!--Display selected items in form for editing -->
								<div class="column">
									<label class="label" for="textInput"><?php echo $row["prod_name"]?> </label>
									<div class="select">
										<select>
											<option value="active">Active</option>
											<option value="inactive">Inactive</option>
										</select>
									</div>
									<form method="post" name="updateForm" action="update.php" >
							        	<div class="control">
							        		<span class="has-text-grey">Product ID</span>
											<input type="text" class="input" name="prodID" value ="<?php echo $row["prod_id"] ?>">
										</div>
										<br>
										<div class="control">
											<span class="has-text-grey">Product Name</span>
											<input type="text" class="input" name="prodName" value ="<?php echo $row["prod_name"] ?>">
										</div>
										<br>
										<div class="control">
											<span class="has-text-grey">Product Description</span>
											<input type="text" class="input" name="prodDesc" value ="<?php echo $row["prod_desc"] ?>">
										</div>
										<br>
										<div class="control">
											<span class="has-text-grey">Product SKU</span>
											<input type="text" class="input" name="prodSku" value ="<?php echo $row["prod_sku"] ?>">
										</div>
										<br>
										<div class="control">
											<span class="has-text-grey">Product Price</span>
											<input type="text" class="input" name="prodPrice" value ="<?php echo $row["prod_price"] ?>">
										</div>
										<br>
										<input type="submit" name="action" value="Update" class="button is-primary is">
										<input type="submit" name="action" value="Delete" class="button is-primary is">
									</form>
								</div>

								<br>
						</div>
						<hr>
					</div>
					<!-- close of while loop above -->
					<?php
						}
						// displ 0 if no results found from db
						} else {
			   		 		echo "0 results";
								}
					?>
			</main>
		</div>		
	</section>
<script type="text/javascript">
// Drop down menu from burger
	(function() {
		var burger = document.querySelector('.burger');
		var nav = document.querySelector('#'+burger.dataset.target);
		burger.addEventListener('click',function(){
			burger.classList.toggle('is-active');
			nav.classList.toggle('is-active');
		});
	})();
</script>

</body>


</html>