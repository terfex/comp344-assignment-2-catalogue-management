<!-- Page not implemented -->
<!DOCTYPE html>
<html lang="en" class="has-background-primary">
<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>ASS2</title>
		<link rel="stylesheet" 
        href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.0/css/bulma.min.css">
        <link rel="stylesheet" href="css/bulma.css" type="text/css"/>
		<script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
       <!--  <link rel="stylesheet" href="css/debug.css"> -->
	<style>
		.center {
			display: flex;
			justify-content: center;
			align-items: center;
		}

		.emoticon { 
			white-space: pre; 
		}
	</style>
</head>
<body>
	<nav class="navbar" role="navigation" aria-label="main navigation">
		<div class="navbar-brand">
		    <a class="navbar-item" href="https://bulma.io">
		    	<!-- Spot to put company logo if needed -->
		    	<img  width="50" height="60">
		    </a>
		    <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="mainNavbar">
		      <span aria-hidden="true"></span>
		      <span aria-hidden="true"></span>
		      <span aria-hidden="true"></span>
		    </a>
		</div>

	    <div id="mainNavbar" class="navbar-menu">
			<div class="navbar-start">
	      		<a class="navbar-item" href="index.php">Current Items</a>
		        <a class="navbar-item" href="addNew.php">Add Item</a>
			  	<a class="navbar-item" href="categories.php">Categories </a>
			  	<a class="navbar-item" href="addCategory.php">Add Categories</a>
			  	<a class="navbar-item" href="shopper.php">Shopper </a>
			  	<a class="navbar-item" href="product.php">Product </a>
		    </div>

		    <div class="navbar-end">
		      <div class="navbar-item">
		        <div class="buttons">
		          <a class="button is-light">Log out</a>
		        </div>
		      </div>
		    </div>
		</div>
	</nav>

	<section class="section">
		<div class="container">
			<h1 class="title is-1 has-text-white">Discounts</h1>
			
			<main>
				<!-- Item 1 -->
				<div class="card-content has-background-white">
					<div class="columns">
						<div class="column">
							<label class="label" for="textInput">Discount Name</label>
								<div class="control">
									<div class="select">
										<select>
											<option value="active">Active</option>
											<option value="inactive">Inactive</option>
										</select>
									</div>
								</div>
								<div class="control">
									<input type="text" class="input" id="prodCode" placeholder="Discount Name">
								</div>
								<br>
								<div class="control">
									<input type="text" class="input" id="prodPrice" placeholder="Discount Code">
								</div>
								<br>
								<div class="control">
									<textarea class="textarea" placeholder="Discount description"></textarea>
								</div>
								<button type="submit" class="button is-primary is">Update</button>
						</div>
						<br>
					</div>
				</div>
				
			</main>
		</div>		
	</section>







<script type="text/javascript">
// Drop down menu from burger
	(function() {
		var burger = document.querySelector('.burger');
		var nav = document.querySelector('#'+burger.dataset.target);
		burger.addEventListener('click',function(){
			burger.classList.toggle('is-active');
			nav.classList.toggle('is-active');
		});
	})();
</script>

</body>


</html>