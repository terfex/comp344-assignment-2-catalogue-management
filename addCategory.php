<!DOCTYPE html>
<html lang="en" class="has-background-primary">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>ASS2</title>
	<link rel="stylesheet" 
        href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.0/css/bulma.min.css">
    <link rel="stylesheet" href="css/bulma.css" type="text/css"/>
	<script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
	<style>
	/*styling calls to be used throughout*/
		.center {
			display: flex;
			justify-content: center;
			align-items: center;
		}

		.emoticon { 
			white-space: pre; 
		}
	</style>
</head>
<body>
	<?php
	//db connect and disconnect calls
		include("dbconnect.php"); 
		include("dbdisconnect.php"); 		
	?> 
	<nav class="navbar" role="navigation" aria-label="main navigation">
		<div class="navbar-brand">
		    <a class="navbar-item" href="https://bulma.io">
		    	<!-- Spot to put company logo if needed -->
		    	<img  width="50" height="60">
		    </a>
		    <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="mainNavbar">
		      <span aria-hidden="true"></span>
		      <span aria-hidden="true"></span>
		      <span aria-hidden="true"></span>
		    </a>
		</div>

	    <div id="mainNavbar" class="navbar-menu">
	    	<!-- RBAC to show/hide navbar items based on user security  -->
			<div class="navbar-start">
	      		<a class="navbar-item" href="index.php">Current Items</a>
		        <a class="navbar-item" href="addNew.php">Add Item</a>
			  	<a class="navbar-item" href="categories.php">Categories </a>
			  	<a class="navbar-item" href="addCategory.php">Add Categories</a>
			  	<a class="navbar-item" href="shopper.php">Shopper </a>
			  	<a class="navbar-item" href="product.php">Product </a>
		    </div>

		    <div class="navbar-end">
		      <div class="navbar-item">
		        <div class="buttons">
		          <a class="button is-light">Log out</a>
		        </div>
		      </div>
		    </div>
		</div>
	</nav>

	<section class="section">
		<div class="container">
			<h1 class="title is-1 has-text-white"> Add New Items</h1>
			<main>
				<!-- Styling -->
				<div class="card-content has-background-white">
					<div class="columns">
						<div class="column">
							<label class="label" for="textInput">New Category</label>
							<div class="control">
								<!-- Add categories form -->
								<form method="post" name="addCatForm" action="addCat.php" >
									<div class="control">
										<input type="text" class="input" name="catID" placeholder=" Category ID" required="required" maxlength="6" minlength="6">
									</div>
									<br>
									<div class="control">
										<input type="text" class="input" name="catName" placeholder=" Category Name" required="required">
									</div>
									<br>
									<div class="control">
										<input type="text" class="input" name="catDesc" placeholder=" Category Description" required="required">
									</div>
									<br>
									<div class="control">
										<input type="submit" class="button is-primary is" value="Add">
									</div>	
								</form>
							</div>
						</div>
					</div>
				</div>
			</main>
		</div>		
	</section>
<script type="text/javascript">
// Drop down menu from burger
	(function() {
		var burger = document.querySelector('.burger');
		var nav = document.querySelector('#'+burger.dataset.target);
		burger.addEventListener('click',function(){
			burger.classList.toggle('is-active');
			nav.classList.toggle('is-active');
		});
	})();
</script>
</body>
</html>